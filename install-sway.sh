#!/bin/bash

ln -sf /usr/share/zoneinfo/America/Chicago/etc/localtime
hwclock --systohc
vim /etc/locale.gen #find your locale and uncomment it
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
echo "monke" >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 monke.localdomain monke" >> /etc/hosts

pacman -S grub efibootmgr networkmanager network-manager-applet wpa_supplicant wireless_tools openssh base-devel linux-zen-headers dialog os-prober mtools dosfstools bluez bluez-tools blueman pipewire pipewire-alsa pipewire-pulse pipewire-jack wireplumber sudo mesa

mkdir -p /boot/efi
mount /dev/sda1 /boot/efi
grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=Arch-Linux
grub-mkconfig -o /boot/grub/grub.cfg

systemctl enable NetworkManager
sudo systemctl start bluetooth.service
sudo systemctl enable bluetooth.service

passwd

useradd -m comedy
passwd comedy
usermod -aG wheel,audio,video,optical,storage comedy
EDITOR=vim
visudo #uncomment the %wheel = ALL(ALL = ALL)

pacman -S sway

vim /etc/mkinitcpio.conf #place your device into module section
mkinitcpio -p linux-zen

#reboot
