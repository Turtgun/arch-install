nmtui
sudo timedatectl set-ntp true
sudo hwclock --systohc
sudo vim /etc/pacman.conf
sudo pacman -Syyu
sudo pacman -S \
  alacritty waybar wofi \
  xorg-xwayland xorg-xlsclients qt5-wayland glfw-wayland \
  xdg-desktop-portal-wlr libpipewire02 polkit-gnome wayland-protocols

mkdir ~/.config
cd ~/.config
mkdir sway waybar wofi alacritty
cp -r /arch-install/sway/ .
cp -r /arch-install/waybar/ .
cp -r /arch-install/wofi/ .
echo "background_opacity: 0.8" >> ./alacritty/alacritty.yml

# apps that i use

cd /tmp
git clone https://aur.archlinux.org/yay-git.git
cd yay-git
makepkg -si

sudo pacman -S \
  wget ark blender firefox neofetch \
  man-db pavucontrol dia cups fuse krita \
  virtualbox vulkan-mesa-layers htop feh \
  gparted jre11-openjdk jdk11-openjdk  tmux \
  libreoffice-fresh nmap ranger mpv youtube-dl \
  sfml mako nmap starship gdk-pixbuf2

yay -S \
  cava-git flashplayer-standalone \
  ttf-font-awesome httpfs2-2gbplus ttf-ms-win10-auto \
  sway-interactive-screenshot swaylock-effects-git

cd
echo 'eval "$(starship init bash)"' >> .bashrc
